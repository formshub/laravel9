import './bootstrap'
import { createApp } from 'vue/dist/vue.esm-bundler'
import { createStore } from 'vuex'
import '../style/index.scss'
import router from './routes'
import store from './store'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './components/App.vue'

const Store = createStore(store)

const app = createApp({
    components: {
        App
    }
})
app.use(router)
app.use(Store)
app.use(ElementPlus)
window.vm = app.mount('#app')
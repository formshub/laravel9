import { createRouter, createWebHistory } from 'vue-router'
import routes from './routes'

const router = createRouter({
    history: createWebHistory(),
    routes
})

router.beforeEach((to, from, next) => {
    const isAuth = !!window.vm.$store.state.auth.user

    if (to.meta.isAuthRequire && !isAuth) {
        next({ name: "login" })
    } else {
        next()
    }
})

export default router
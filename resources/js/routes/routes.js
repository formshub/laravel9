import Login from '../components/Login.vue'
import Home from '../components/Home.vue'

export default [
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/',
        name: 'home',
        component: Home
    }
]
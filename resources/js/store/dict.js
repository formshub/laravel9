import axios from 'axios'

export default {
    namespaced: true,
    state() {
        return {
        }
    },
    actions: {
        async load(state, param) {
            try {
                const { data } = await axios.get('/dict/load', {
                    ...param,
                    tag: 'dict'
                })
                state.commit('set', data)
                return data
            } catch (e) {
                throw new Error(e)
            }
        }
    },
    mutations: {
        set(state, data) {
            Object.keys(state).forEach((dictName) => {
                if (data[dictName]) {
                    state[dictName] = data[dictName]
                }
            })
        }
    }
}
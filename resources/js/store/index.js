import auth from './auth'
import dict from './dict'

export default {
    state() {
        return {
            tagLoading: {}
        }
    },
    actions: {},
    mutations: {
        setTagLoading(state, tagList) {
            const set = (tag) => {
                if (!state.tagLoading[tag]) {
                    const obj = {}
                    obj[tag] = 0
                    state.tagLoading = {
                        ...state.tagLoading,
                        ...obj
                    }
                }
                state.tagLoading[tag] += 1
            }

            if (_.isArray(tagList)) {
                tagList.forEach((tag) => {
                    set(tag)
                })
            } else {
                set(tagList)
            }
        },

        unsetTagLoading(state, tagList) {
            const unset = (tag) => {
                state.tagLoading[tag] = state.tagLoading[tag] ? state.tagLoading[tag] -= 1 : 0
            }

            if (_.isArray(tagList)) {
                tagList.forEach((tag) => {
                    unset(tag)
                })
            } else {
                unset(tagList)
            }
        }
    },
    modules: {
        auth,
        dict
    }
}
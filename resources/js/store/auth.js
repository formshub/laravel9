import axios from 'axios'

export default {
    namespaced: true,
    state() {
        return {
            user: null
        }
    },
    actions: {
        async user(state) {
            try {
                const { data } = await axios.get('/auth/user', { tag: 'user' })
                state.commit('setUser', data.user)
                return data
            } catch (e) {
                state.commit('setUser', null)
                // throw new Error(e)
            }
        },
        async login(state, param) {
            try {
                await axios.get('/sanctum/csrf-cookie', { tag: 'auth' }, { baseURL: '/' })
                const { data } = await axios.post('/auth/login', param, { tag: 'auth' })

                state.commit('setUser', data.user)
                return data
            } catch (e) {
                state.commit('setUser', null)
                throw new Error(e)
            }
        },
        async logout(state) {
            try {
                await axios.post('/auth/logout', null, { tag: 'auth' })
                state.commit('setUser', null)
            } catch (e) {
                throw new Error(e)
            }
        },
        async register() {
            try {
                const { data } = await axios.post('/auth/register', param, { tag: 'auth' })

                state.commit('setUser', data.user)
                return data
            } catch (e) {
                state.commit('setUser', null)
                throw new Error(e)
            }
        }
    },
    mutations: {
        setUser(state, data) {
            state.user = data
        }
    }
}
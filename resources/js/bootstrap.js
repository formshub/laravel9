import _ from 'lodash';
import ResponseCode from './dicts/responseCode'

window._ = _;

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

import axios from 'axios';

axios.defaults.baseURL = '/api/';
axios.defaults.withCredentials = true;

axios.interceptors.request.use((config) => {
    if (config.tag) {
        window.vm.$store.commit('setTagLoading', config.tag)
    }

    return config
}, (error) => {
    const newError = error
    return Promise.reject(newError)
})

axios.interceptors.response.use((response) => {
    const newResponse = response

    if (response.request.responseType !== 'blob') {
        newResponse.data = response.data.result
    }

    if (response.config.tag) {
        window.vm.$store.commit('unsetTagLoading', response.config.tag)
    }

    return newResponse
}, (error) => {
    // console.log('error', error)
    const newError = error
    const config = error.response.config || {}

    if (error.response && error.response.data) {
        if (error.response.status === ResponseCode.HTTP_INTERNAL_SERVER_ERROR) {
            window.vm.$message.error({
                // message: `Внутренняя ошибка сервера (${error.response.status})`,
                message: 'Внутренняя ошибка сервера',
                showClose: true,
                duration: 10 * 1000
            })
        } else if (error.response.status === ResponseCode.HTTP_FORBIDDEN) {
            window.vm.$message.error({
                message: 'Недостаточно привилегий для просмотра страницы!',
                showClose: true,
                duration: 10 * 1000
            })
        } else if (config.check404 && error.response.status === ResponseCode.HTTP_NOT_FOUND) {
            window.vm.$router.replace({ name: 'not_found' })
        } else if (error.response.data.result) {
            newError.response.data = error.response.data.result
        } else {
            window.vm.$message.warning({
                // message: `${error.response.data.message} (${error.response.status})`,
                message: error.response.data.message,
                showClose: true,
                duration: 5 * 1000
            })
        }
    }

    if (config.tag) {
        window.vm.$store.commit('unsetTagLoading', error.response.config.tag)
    }

    return Promise.reject(newError)
})

window.axios = axios;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// import Pusher from 'pusher-js';
// window.Pusher = Pusher;

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: import.meta.env.VITE_PUSHER_APP_KEY,
//     wsHost: import.meta.env.VITE_PUSHER_HOST ?? `ws-${import.meta.env.VITE_PUSHER_APP_CLUSTER}.pusher.com`,
//     wsPort: import.meta.env.VITE_PUSHER_PORT ?? 80,
//     wssPort: import.meta.env.VITE_PUSHER_PORT ?? 443,
//     forceTLS: (import.meta.env.VITE_PUSHER_SCHEME ?? 'https') === 'https',
//     enabledTransports: ['ws', 'wss'],
// });

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Обработка попыток аутентификации.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'login' => ['required'],
            'password' => ['required'],
        ]);

        if (!Auth::attempt($credentials)) {
            abort(Response::HTTP_UNAUTHORIZED, 'Неверный логин или пароль');
        }

        $request->session()->regenerate();
        $request->user()->createToken('api');

        return response(['status' => 'success', 'user' => $request->user()]);
    }

    public function register()
    {

    }

    public function user(Request $request)
    {
        return response(['status' => 'success', 'user' => $request->user()]);
    }

    public function logout()
    {
        Auth()->guard('web')->logout();
        return response(['status' => 'success']);
    }
}

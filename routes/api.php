<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/dict/load', "DictController@load");

Route::get('/sanctum/csrf-cookie', function () {
    Route::get(
        '/csrf-cookie',
        \Laravel\Sanctum\Http\Controllers\CsrfCookieController::class . '@show'
    );
});
Route::get('/auth/user', "Auth\AuthController@user");
Route::post('/auth/login', "Auth\AuthController@login");
Route::post('/auth/register', "Auth\AuthController@register");

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('{all}', function () {
    abort(404, "Not found");
})->where(['all' => '.*']);
